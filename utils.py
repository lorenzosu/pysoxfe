def hms_to_sec(hms_string):
    """ Convert string in the format hh:smm:ss.ss to seconds (floating point)"""
    dur_sec = 0.0
    dur_split = hms_string.split(':')
    dur_split.reverse()
    for i in xrange(len(dur_split)):
        dur_sec = dur_sec + float(dur_split[i]) * (60 ** i)
    return dur_sec

def sec_to_hms(seconds):
    """ Convert seconds (float) to hh:mm:ss.ss string (secondds to 1/100) """
    h = seconds // 3600
    m = (seconds - (h * 3600)) // 60
    s = seconds - (h * 3600) - (m * 60)
    return ("%02d:%02d:%02.2f") % (h,m,s)


"""
#TESTS
print "### TEST: h:m:s <-> seconds ####"
########## Handle h:m:s <-> seconds
hms_list = ['00:20:18.5','01:12:12.01','00:12:00.02','00:00:49.1','00:15',
            '04:24.2','34.2']
for hs in hms_list:
    print ('%12s %10f') % (hs, audio_utils.hms_to_sec(hs))

secs_list = [4000, 5000.86, 500, 20, 10.5,]   
for sec in secs_list:
    print ('%12f ---> %s') % (sec, audio_utils.sec_to_hms(sec))
    
"""    
