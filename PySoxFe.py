#!/usr/bin/env python
# Filename PySoxFe.py
"""
    PySoxFe a Python Sox FrontEnd. (using subprocess)

    Works on Linux (maybe also others)
    Requires Sox with mp3 *input* support and lame
"""
import os
#import sys
import subprocess
import shlex

import check_open2
import utils

# Works only on linux if they are installed...

__version__ = (0, 1)
__lame_path__ = 'lame'
__sox_path__ = 'sox'
__soxi_path__ = 'soxi'

__binaries__ = {
    'SoX, Sound eXchange format - http://sox.sourceforge.net/': __sox_path__,
    'SoXi, Sound eXchange format Information - http://sox.sourceforge.net/':
        __soxi_path__,
    'LAME MP3 Encoder - http://lame.sourceforge.net/': __lame_path__
    }
# search for binaries in current dir and system paths.. Works on linux only!!
__search_paths__ = ['.'] + os.getenv ('PATH').split (':')
for name, binary in __binaries__.iteritems ():
    found_binary = False
    for path in __search_paths__:
        check_path = path + os.path.sep + binary
        if (os.path.exists (check_path)):
            #print "Found %s binary: %s" % (name, check_path)
            found_binary = True
            break
    if not found_binary:
        error_message = ("""
%s with binary "%s" is required.
Is it installed? If yes please make sure it is in current path.
If not please installe it.
""") % (name, binary)
        raise ImportError, error_message
# check that sox has mp3 read support

out_string = subprocess.check_output(['sox','-h'])
out_string = out_string[out_string.find("AUDIO FILE FORMATS:"):]
out_string = out_string[:out_string.find("\n")]
if out_string.find("mp3") >= 0:
    # OK
    pass
else:
    error_message = ("""

SoX installed but does not seem to support mp3 read.
On debian-like system this is provided by something like libsox-fmt-mp3.
Please ensure mp3 support in SoX is installed.
""")
    raise ImportError, error_message

__errors__ = {
    1:'Generic error',
    2:'subprocess.Popen Error',
    4:'SoX Error',
    3:'SoXi Error',
    5:'Lame Error'
}

def soxi_parser (soxi_string):
    """
    Parse a string as returned by soxi and return a dictionary with
    file info.
    """
    lines = soxi_string.split("\n")
    info_dic = {}
    for li in lines:
        #print li
        if li.find(':') >= 0:
            values = li.split(':', 1)
        elif li.find('=') >= 0:
            values = li.split('=', 1)
        else:
            continue
        if values[0] != '':
            info_dic[values[0].rstrip()] = values[1].lstrip()
    return info_dic

class TrimData:
    """
    Class defining data for the Mp3File.do_trim() method. All times are
    in seconds.
    Calling the check method will perform some consistency checks on the
    data (e.g. end is > than start etc.)

    The values are (with defaults - numbers in seconds):
        self.start_trim = 0
        self.end_trim = 60
        self.do_fade = False
        self.fade_in_dur = 0
        self.fade_out_begin = 50
        self.fade_out_dur= 10
        self.lame_bitrate = None # leave unchanged
"""
    def __init__(self):
        """ Intialize TrimData with default values """
       # numbers are in seconds
        self.start_trim = 0.0
        self.end_trim = 60.0
        self.do_fade = False
        self.fade_in_dur = 0.0
        self.fade_out_dur = 10.0
        self.lame_bitrate = None

    def check(self):
        """ Check for values' consistency raising ValueError on errors """
        st_end_string = ("%f ~ %f") % (self.start_trim, self.end_trim)
        if self.start_trim < 0:
            raise ValueError, ("start_trim must be >= 0\n%s") % (st_end_string)
        if self.fade_in_dur > self.end_trim:
            raise ValueError, "fade_in_dur must be <= end_trim"
        #if self.fade_out_dur > (self.end_trim - self.start_trim):
         #   raise ValueError, "fade_out_dur must be <= than trimmed length"

        
class AudioFile:
    """ Class with utilities to work on audio files files using Lame and Sox """
    def __init__ (self, the_file):
        # set_file will raise an IOError exception if the file does not exist
        self.the_file = ''
        self.file_ok = False
        self.set_file(the_file)
        self.last_error = 0
   
    def set_file (self, the_file):
        """ Set the mp3 file """
        if not os.path.exists(the_file):
            error_msg = "mp3_file Error: %s does not exist" % (the_file)
            self.last_error = 1
            self.the_file = ''
            self.file_ok = False
            raise IOError, error_msg
        else:
            self.the_file = the_file
            self.last_error = 0
            self.file_ok = True
        return self.last_error

    def get_info (self):
        """
        Use soxi to get info about the file. We use subprocess.check_output,
        so in case of error a CalledProcessError exception will be raised.
        """
        #info = {}
        soxi_command = ('%s -V0 %s') % (__soxi_path__, self.the_file)
        soxi_args = shlex.split(soxi_command)
        soxi_output = check_open2.check_open2(soxi_args)
        #print soxi_output
        return soxi_parser(soxi_output)

    def get_secs (self):
        """ Get duration in seconds by parsing output of get_info() """
        info_dic = self.get_info()
        if info_dic == None:
            return -1
        dur_string = info_dic['Duration'].split('=', 1)[0].rstrip()
        return utils.hms_to_sec(dur_string)

    def trim(self, trim_data, out_file):
        """ Trim an audio file """
        total_seconds = self.get_secs()
        info_dic = self.get_info()
        # new_length = trim_data.end_trim - trim_data.start_trim
        if trim_data.do_fade == True:
            fade_part =  ('fade t %s %s %s') % (
                                (trim_data.fade_in_dur),
                                (trim_data.end_trim) ,
                                (trim_data.fade_out_dur)
                            )
        else:
            fade_part = ''
        # endif
        if trim_data.start_trim > total_seconds:
            print "Warning trim start bigger than file length! NOT trimming"
            trim_data.start_trim = 0
        # Use one of the trim methods depending on the output extension
        if os.path.splitext(out_file)[1] == '.mp3':
            return self.mp3_trim(trim_data, info_dic, fade_part, out_file)
        else:
            return self.other_trim(trim_data, info_dic, fade_part, out_file)

    def mp3_trim(self, trim_data, info_dic, fade_part, out_file):
        """
        Trim to mp3 file, optionally adding a fade-in and/or fade-out and
        output to out_file. trim_data is of class TrimData holding the data.
        """ 
        sox_command = ('%s -V0 %s -t raw - trim %s %s %s') % (
                        __sox_path__,
                        self.the_file,
                        utils.sec_to_hms(trim_data.start_trim),
                        utils.sec_to_hms(trim_data.end_trim),
                        fade_part
                )
        print sox_command
        sox_args = shlex.split(sox_command)
        raw_file = check_open2.check_open2(sox_args)
        # TODO what happens with a very big file? How big is 'very' big?

        if trim_data.lame_bitrate != None:
            bitrate_part = ("-b %d") % (trim_data.lame_bitrate)
        else:
            bitrate_part = ''
        if info_dic['Channels'] == '1':
            chan_part = ("-m m")
        else:
            chan_part = ("-m s")
        srate_part = ("-s %.1f") % (float(info_dic['Sample Rate']) / 1000.)
        #print srate_part
        lame_command = (('lame - -r --quiet %s %s %s %s') %
            (
                    chan_part,
                    srate_part,
                    bitrate_part,
                    out_file
            )
        )
        lame_args = shlex.split(lame_command)
        #lame_output =
        check_open2.check_open2(lame_args, stdin_string=raw_file)
        self.last_error = 0
        return self.last_error

    def other_trim(self, trim_data, info_dic, fade_part, out_file):
        """
        Trim to other supported file, optionally adding a fade-in and/or
        fade-out and output to out_file. trim_data is of class TrimData holding the data.
        """ 
        sox_command = ('%s -V0 %s %s trim %s %s %s') % (
                        __sox_path__,
                        self.the_file,
                        out_file,
                        utils.sec_to_hms(trim_data.start_trim),
                        utils.sec_to_hms(trim_data.end_trim),
                        fade_part
                )
        print sox_command
        sox_args = shlex.split(sox_command)
        check_open2.check_open2(sox_args)
        # TODO what happens with a very big file? How big is 'very' big?
        self.last_error = 0
        return self.last_error
