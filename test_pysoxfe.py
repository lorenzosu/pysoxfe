#!/usr/bin/env python
""" Tests for PySoxFe """
import subprocess
import os
import PySoxFe

test_file_path = './audio_test/test_sin.mp3'
test_file_out = './audio_test/out_test.mp3'
test_file_flac = './audio_test/test_tones.flac'
test_file_out_ogg = './audio_test/out_test.ogg'
wrong_file = './audio_test/not_an_audio_file.mp3'

print "### TEST: CLASS CREATION ####"
######### Class creation with input file ######
trim_d = PySoxFe.TrimData()
trim_d.start_trim = 0
trim_d.end_trim = 10
trim_d.do_fade = True
trim_d.fade_in_dur = 2
trim_d.fade_out_dur = 3
trim_d.lame_bitrate = None
trim_d.check()

m = PySoxFe.AudioFile(test_file_path)

print "### TEST: TRIMMING ####"
###### Mp3 TRIMMING #####
print "---> Removing old test file"
try:
    os.remove(test_file_out)
    os.remove(test_file_out_ogg)
except OSError as error:
    pass

m.trim(trim_d, test_file_out)
print "OK"

print "### TEST: GET INFO ####"
####### Get info #######
s = m.get_info()
print s
print "Seconds:", m.get_secs()

print 'TEST: FLAC FILE to OGG\n'
####### Flac file to ogg #######
m.set_file(test_file_flac)
m.trim(trim_d, test_file_out_ogg)

print "### TEST: ERRORS AND EXCEPTIONS ####"
####### Errors and exceptions ######
# Test non-existing file handling
try:
    m.set_file('non_existing_file.mp3')
except IOError as error:
    print "EXCEPTION caught: %s" % (error)

# Test soxi error (doing get_info on non-audio file)
m.set_file(wrong_file)
try:
    og = m.get_info()
except subprocess.CalledProcessError as error:
    print "EXCEPTION caught: %s" % (error)
