""" Provides check_output alternative """
import subprocess
def check_open2 (args, stdin_string=''):
    """
    Similar to Python 2.7 subprocess.check_output but with optional stdin pipe.
    Runs subprocess.Popen with stdout always piped.
    If stdin_string is different than '' it will aslo be piped as stdin.
    If the return code is != 0 raise a subprocess.CalledProcessError exception.
    Return the command output if successful.
    """
    if stdin_string == '':
        process = subprocess.Popen(args, stdout=subprocess.PIPE)
        process_output = process.communicate()
    else:
        process = subprocess.Popen(
                                args,
                                stdout=subprocess.PIPE,
                                stdin=subprocess.PIPE
                                )
        process_output = process.communicate(stdin_string)

    process_ret_code = process.returncode
    if process_ret_code != 0:
        raise subprocess.CalledProcessError(process_ret_code, ' '.join(args))
    return process_output[0]
